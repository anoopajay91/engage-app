from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from tag.views import index
admin.autodiscover()

handler404 = 'tag.views.error_page'
handler500 = 'tag.views.error_page'

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'engage.views.home', name='home'),
    # url(r'^engage/', include('engage.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^$', index, name="index"),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tag/', include('tag.urls',namespace='tag')),
)
