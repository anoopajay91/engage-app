from django.db import models
from django.contrib.auth.models import User
from djangotoolbox.fields import ListField
from django_mongodb_engine.contrib import MongoDBManager


# Create your models here.
class UserProfile(models.Model):
	user = models.OneToOneField(User, unique=True, null=True)
	friend_list = ListField()
	full_name = models.CharField(max_length=200)

class Posts(models.Model):
	post = models.CharField(max_length=1200)
	created_by = models.CharField(max_length=200)
	mentions = ListField()