# Create your views here.

from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.contrib.auth import login, logout
from tag.models import *
from django.contrib.auth.models import User
import json
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

from pymongo import	MongoClient
client = MongoClient()
db = client.engage

# main page
def index(request):
	return redirect('/tag/')

# tag application index / login page
def tag_index(request):
	context = RequestContext(request,{})
	if request.user.is_authenticated():
		return redirect('/tag/posts')
	else:
		return render_to_response('index.html', context_instance=context)

# signup function when signup is clicked
def signup(request):
	username = request.POST.get('email','')
	first_name = request.POST.get('first_name','')
	last_name = request.POST.get('last_name','')
	friend_list = request.POST.getlist('friend_list[]','')

	exists = User.objects.filter(username__exact=username).exists()
	
	if exists:
		user = User.objects.get(username=username)
		user_profile = UserProfile.objects.get(user_id=user.pk)
		user_profile.full_name = user.first_name+ " " + user.last_name
		user_profile.friend_list = friend_list
		user_profile.save()
	else:
		user = User.objects.create_user(username, username)
		user.first_name = first_name
		user.last_name = last_name
		user.save()

		full_name = user.first_name+ " " + user.last_name
		user_profile_exists = UserProfile.objects.filter(full_name=full_name).exists()

		if user_profile_exists:
			user_profile = UserProfile.objects.get(full_name=full_name)
			user_profile.user_id = user.pk
		else:
			user_profile = UserProfile(user_id=user.pk)

		user_profile.full_name = full_name
		user_profile.friend_list = friend_list
		user_profile.save()

	if request.user.is_authenticated():
		return HttpResponse(json.dumps({'url':'/tag/posts'}),mimetype='application/json')
		
	user.backend = 'django.contrib.auth.backends.ModelBackend'
	login(request,user)
	return HttpResponse(json.dumps({'url':'/tag/posts'}),mimetype='application/json')

# post page to make posts
def posts(request):
	email = request.user.email
	user = User.objects.get(email=email)
	user_profile = UserProfile.objects.get(user_id=user.pk)
	
	friends = json.dumps(user_profile.friend_list)

	context = RequestContext(request,{'friends':friends})
	return render_to_response('posts.html', context_instance=context)

# signout function when signout is called
def signout(request):
	logout(request)
	return redirect('/tag')

# to create posts
def create_post(request):
	if request.method == "POST":
		user = User.objects.get(email = request.user.email)
		user_profile_id = UserProfile.objects.get(user_id=user.pk).id

		mentions = request.POST.getlist('mentions[]','')
		post = request.POST.get('post','')

		post = Posts(post=post, created_by=user_profile_id)
		mentions_list = []

		print mentions
		for mention in mentions:
			exists = UserProfile.objects.filter(full_name=mention).exists()
			if exists:
				## fetch
				user_profile = UserProfile.objects.get(full_name=mention)
			else:
				## create
				user_profile = UserProfile(full_name = mention)
				user_profile.save()
			mentions_list.append(user_profile.id)

		post.mentions = mentions_list
		post.save()

		return HttpResponse(json.dumps({'added':True}), mimetype='application/json')
	else:
		return HttpResponse(json.dumps({'message':'invalid call to url has been made'}), 
										mimetype='application/json')


# to show mentioned posts
def show_posts(request):
	user = User.objects.get(email = request.user.email)
	user_profile_id = UserProfile.objects.get(user_id=user.pk).id

	results = Posts.objects.filter(mentions=user_profile_id).values('post')

	context = RequestContext(request, {'mentions':results, 'title':'Mentioned posts'})
	return render_to_response('show-mentions.html',context)

# to show user's posts
def my_posts(request):
	user = User.objects.get(email = request.user.email)
	user_profile_id = UserProfile.objects.get(user_id=user.pk).id
	results = Posts.objects.filter(created_by=user_profile_id).values('post')

	context = RequestContext(request, {'mentions':results, 'title':'My posts'})
	return render_to_response('show-mentions.html',context)


def error_page(request):
	context = RequestContext(request, {})
	return render_to_response('error.html',context)