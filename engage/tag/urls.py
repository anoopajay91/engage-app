from django.conf.urls import patterns, include, url
import views

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'engage.views.home', name='home'),
    # url(r'^engage/', include('engage.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.tag_index, name='tag_index'),
    url(r'^posts/$', views.posts, name='posts'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^signout/$', views.signout, name='signout'),
    url(r'^create-post/$', views.create_post, name='create-post'),
    url(r'^show-mentions/$', views.show_posts, name='show-post'),
    url(r'^my-posts/$', views.my_posts, name='my-posts'),  

)